//Sample test code without BDD
package org.info.pages;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import io.restassured.response.Response;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import org.json.simple.JSONObject;

public class SampleClass {
	@Test
	void test01() {
		Response response=get("https://reqres.in/api/users?page=2");
		System.out.println("Status code is " +response.getStatusCode());
		System.out.println("Response Body is "+response.getBody());
		System.out.println("Response as String "+response.asString());
	}
	
	@Test
	void test02() {
		given().get("https://reqres.in/api/users?page=2").then().statusCode(200);
		get("https://reqres.in/api/users?page=2").then().body("data.id[0]",equalTo(7));
		
	}
	@Test
	void test03() {
		JSONObject request=new JSONObject();
		request.put("name", "test");
		request.put("job", "tester");
		System.out.println("Request is " +request);;
		given().
		body(request.toJSONString()).
		when().
		post("https://reqres.in/api/users").
		then().statusCode(201);
		
	}
}
