package org.info.steps;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.json.simple.JSONObject;
import org.openqa.selenium.firefox.FirefoxDriver;
import cucumber.api.java.en.*;
import io.restassured.response.Response;

public class StepDefinition {

	@Given("user login to the system")
	public void user_is_loggingIn() throws Throwable {
		// code to hit login api
		/*
		 * This is sample code Response
		 * response=get("https://reqres.in/api/users?page=2");
		 * System.out.println("Status code is " +response.getStatusCode());
		 * System.out.println("Response Body is "+response.getBody());
		 * System.out.println("Response as String "+response.asString()); JSONObject
		 * request=new JSONObject(); request.put("name", "test");
		 * request.put("job","tester"); System.out.println("Request is " +request);;
		 * given().body(request.toJSONString()). when().
		 * post("https://reqres.in/api/users"). then().statusCode(201);
		 */

	}

	@Then("status code should be 200")
	public void user_verifies_statuscode() throws Throwable {
		// code to verify response code
		/*
		 * This is sample code
		 * given().get("https://reqres.in/api/users?page=2").then().statusCode(200);
		 * get("https://reqres.in/api/users?page=2").then().body("data.id[0]",equalTo(7)
		 * );
		 */

	}
}
