/*This is runner file to run all the tests*/

package org.info.runners;

import org.junit.runner.RunWith;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/functionalTests",
		glue = {
		"org.info.steps" },
		monochrome = true, 
		strict = true)
public class TestRunner {

}
